{/* <div class="productCard">
                <img class="productThumbnail"src="https://images.unsplash.com/photo-1716343168811-353d613d132a?q=80&w=1974&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"/>
                <div class="productBottomSheet">
                    <div class="productInfoContainer">
                        <strong class="producName">iPhone</strong>
                        <div class="productPrice">$200</div>
                    </div>
                    <button class="addToCart">+</button>
                </div>
            </div> */}
let products =[];
const cart ={};

const updateCart =()=>{
    let totalPrice =0;
    document.querySelector('#cartSummary_items').replaceChildren([]);

    for(const key of Object.keys(cart)){
        const item = products.find((product)=>{
            return `${product.id}`===key;
        });

        console.log(key);
        console.log(products);

        const quantity = cart[key];
        const price= item.price;
        const itemRow = document.createElement('tr');
        
        const itemName = document.createElement("th");
        itemName.innerText = item.title;

        const itemQuantity =document.createElement('td');
        itemQuantity.innerText=quantity;

        const itemPrice = document.createElement('td');
        itemPrice.innerText =quantity*price;

        itemRow.append(itemName,itemQuantity,itemPrice);
        document.querySelector('#cartSummary_items').append(itemRow);

        totalPrice= totalPrice+price*quantity;
    }
    document.querySelector('#cartSummary_total').innerText=totalPrice;
}

const createCard =(product)=>{
    const productCart = document.createElement('div');
    productCart.className='productCard';

    const productThumbnail = document.createElement('img');
    productThumbnail.className= 'productThumbnail'
    productThumbnail.src= product.thumbnail;
    
    const productBottomSheet = document.createElement('div');
    productBottomSheet.className ='productBottomSheet'

    const productInfoContainer = document.createElement('div');
    productInfoContainer.className = 'productInfoContainer';

    const producName =document.createElement('strong');
    producName.className='producName';
    producName.innerText=product.title;

    const productPrice = document.createElement('div');
    productPrice.className = 'productPrice';
    productPrice.innerText='$'+ product.price;

    const addToCart = document.createElement('button');
    addToCart.className='addToCart';
    addToCart.innerText=`+`;

    const delToCart = document.createElement('button');
    delToCart.className='delToCart';
    delToCart.innerText=`-`;

    addToCart.addEventListener('click',()=>{
        if (cart[product.id] === undefined) cart[product.id]=0;
        cart[product.id]=cart[product.id]+1;

        updateCart();
    })

    delToCart.addEventListener('click',()=>{
        if (cart[product.id] === undefined) cart[product.id]=0;
        cart[product.id]=cart[product.id]-1;

        updateCart();
    })


    productInfoContainer.append(producName,productPrice);
    productBottomSheet.append(productInfoContainer,addToCart,delToCart);
    productCart.append(productThumbnail,productBottomSheet);

    document.querySelector('#productList').appendChild(productCart);
};

const hookViewCart =()=>{
    const viewCartButton = document.querySelector('#viewCart');
    viewCartButton.addEventListener('click',()=>{
        const cartSummary = document.querySelector('#cartSummary');
        const display = cartSummary.style.display;

        if(display === 'none'){
            cartSummary.style.display = 'block'; 
        }else{
            cartSummary.style.display='none';
        }
    });
}

const fetchProduct =()=>{
    fetch('https://dummyjson.com/products')
.then(res => res.json())
.then((productResponse) =>{
    products=productResponse.products;

    products.forEach(product => {
        createCard(product);
        
    });
    console.log(products)
    });   
}


fetchProduct();
hookViewCart();